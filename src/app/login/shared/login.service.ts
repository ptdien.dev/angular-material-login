import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { USERS } from 'src/app/user/shared/mock-users';
import { User } from 'src/app/user/shared/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  getUsers(): Observable<User[]> {
    return of(USERS);
  }

  handleError(e) {
    console.log(e);
  }

  authenticate(user: User, callBack: (result: User) => void) {
    this.getUsers().subscribe((users: User[]) => {
      const validUser = users.find((thisUser: User) => thisUser.username === user.username && thisUser.password === user.password);
      callBack(validUser);
    }, this.handleError);
  }
}
