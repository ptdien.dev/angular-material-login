import { LoginService } from './shared/login.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../user/shared/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  hide = true;
  control = {};
  usernameControl: FormControl;
  passwordControl: FormControl;
  controlGroup: FormGroup;
  constructor(private loginService: LoginService, private router: Router) {
    this.usernameControl = new FormControl('');
    this.passwordControl = new FormControl('');
    this.controlGroup = new FormGroup({
      usernameControl: this.usernameControl,
      passwordControl: this.passwordControl
    });
  }

  onInputChange(control: FormControl, event: Event) {
  }

  onSubmit() {
    this.loginService.authenticate({ username: this.usernameControl.value, password: this.passwordControl.value }
      , (user: User) => {
        if (user) {
          this.router.navigate(['/home'], { queryParams: { name: user.username } });
        }
      });
  }

  ngOnInit() {
  }

}
