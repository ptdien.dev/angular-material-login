import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

const matchTheme = name => {
  return (
    {
      basic: 'basic-theme',
    }[name] || 'default'
  );
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  theme: string;
  name: string;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    const name = this.route.snapshot.queryParamMap.get('name');
    this.name = name;
    this.theme = matchTheme(name);
  }
}
